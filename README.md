
PARROT-DARK-THEMES
===============================

Debian Installation
---------------------- 

Install PARROT-DARK-THEMES step by step ::

    $ chmod 755 install.sh
    
    $ sudo ./install.sh
    
-------------------------------------------------------------------------------    
And You can install this themes by cloning the `Git Repo <https://gitlab.com/hairoes/maia-dark>`_ and simply installing its requirements::


    $ git clone https://gitlab.com/hairoes/maia-dark.git
    
    $ cd maia-dark/

    $ chmod 755 install.sh 
    
    $ sudo ./install.sh
    

========================= edited by nYuYa ==============================
